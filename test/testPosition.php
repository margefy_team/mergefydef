<?php

 //TestPosition Class
 require_once "../entity/Position.php";
 echo "Position class test<br>";
 $p1 = new Position(44.482355, 11.274891);
 $p2 = new Position(44.478246, 11.277379);

 print_r( $p1->getCoordinates() );
 echo "<br>";
 print_r( $p2->getCoordinates() );
 echo "<br>";

 echo "distance in meters: ".$p1->calculateDistance($p2)."<br>";
 echo "is nearer than 4km? ".$p1->isInRadius($p2, 4000)."<br>";

 $p3 = new Position(44.428246, 11.077379);
 echo "distance in meters: ".$p1->calculateDistance($p3)."<br>";
 echo "is nearer than 4km? ".$p1->isInRadius($p3, 4000)."<br>";



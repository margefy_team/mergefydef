<?php
/**
 * Class User
 *
 * Contains information about users.
 *
 * User: filippoboiani
 * Date: 24/01/16
 * Time: 17:40
 */
include_once ('model.class.php');
include_once ('database.php');

class User extends Model{
    private $id;
    private $name, $lastname;
    private $born;
    private $subscriptiondate;
    private $type;
    private $profile_pic;
    private $position;
    private $password;
    private $mail;

    /**
     * User constructor.
     * @param $id
     * @param $name
     * @param $lastname
     * @param $born
     * @param $subscriptiondate
     * @param $type
     * @param $profile_pic
     * @param $position
     * @param $password
     * @param $mail
     */
    /*public function __construct($id, $name, $lastname, $born, $subscriptiondate, $type, $profile_pic, $position, $password, $mail)
    {
        $this->id = $id;
        $this->name = $name;
        $this->lastname = $lastname;
        $this->born = $born;
        $this->subscriptiondate = $subscriptiondate;
        $this->type = $type;
        $this->profile_pic = $profile_pic;
        $this->position = $position;
        $this->password = $password;
        $this->mail = $mail;
    }*/

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @return mixed
     */
    public function getBorn()
    {
        return $this->born;
    }

    /**
     * @return mixed
     */
    public function getSubscriptiondate()
    {
        return $this->subscriptiondate;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getProfilePic()
    {
        return $this->profile_pic;
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return mixed
     */
    public function getMail()
    {
        return $this->mail;
    }

    public function insertUser($param)
    {
        // DB call
        $sql = "insert into user(name, lastname, born, subscriptiondate,password, mail) values('".$param['name']."', '".$param['lastname']."', '1993-02-01', CURRENT_TIMESTAMP(), '".md5($param['password'])."', '".$param['email']."')";

        $db = new Database();
        $db = $db->getConn();
        $db->query($sql);


        //GET THE ID IN ORDER TO SET SESSION PARAMETERS
        $sql = "SELECT id FROM user WHERE mail='" . $param['email'] . "'";

        $db = new Database();
        $db = $db->getConn();
        $userId = $db->query($sql)->fetch(PDO::FETCH_ASSOC);

        //Set the SESSION PARAMETER
        $_SESSION['userId'] = $userId['id'];
        //setcookie('userId', $userId['id'], time() + (86400 * 30), "/");



        echo "/// function /// " . "DONE   " . $userId['id'] . "   rr";
    }

    public function loginUser($param){

        session_start();
        //Check if it is already logged in
        if(isset($_SESSION['userId']) || $_SESSION['userId']!=null){
            //he is already logged
        } else {
            //Check if user exist
            $sql = "SELECT id FROM user WHERE mail='" . $param['mail'] ."' AND password='". md5($param['password'])."'";

            $db = new Database();
            $db = $db->getConn();
            $userId = $db->query($sql);

            if($userId->rowCount()<1 || $userId->rowCount()>1){
                echo "SOMETHING WRONG";
            } else {
                $userId = $userId->fetch(PDO::FETCH_ASSOC);
                $_SESSION['userId'] = $userId['id'];
                echo "DONE";
            }
        }
    }
}


<?php

require_once "config/database.config.php";

class Database extends PDO{
    private $conn= null;
    //Make connection
    public function __construct(){
        $this->conn = new PDO(DB_HOST, DB_USER, DB_PASSWORD);

        try{
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }catch (PDOException $e){
            die($e->getMessage());
        }
    }

    public function getConn(){
        return $this->conn;
    }

    public function run($query){

        //Create a prepared statement
        $stmt = $this->conn->prepare($query);

        if($stmt){
            $stmt->execute();
            return $stmt;
        } else {
            return $this->conn->get_error();
        }

    }

    //Display error
    public function get_error(){
        $this->conn->errorInfo();
    }

    //Closes the database connection when object is destroyed
    public function __destruct()
    {
        $this->conn = null;
    }
}
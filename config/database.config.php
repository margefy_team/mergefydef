<?php

//Define the host used to access the database
define('DB_HOST', 'mysql:host=localhost;dbname=shared_events_v1');

//Define the username to access the database
define('DB_USER', 'root');

//define the password of the username
define('DB_PASSWORD', 'root');

//define the name of the database I want to use
define('DB__NAME', 'shared_events_v1');

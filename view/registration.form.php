<form id="registration" method="post" action="controller.php?controller=user&action=registration">
    <fieldset>
        <label for="name">Name</label>
        <input type="text" id="name" name="name"  placeholder="Insert you name..."/>
    </fieldset>
    <fieldset>
        <label for="lastname">Lastname</label>
        <input type="text" id="lastname" name="lastname"  placeholder="Insert your lastname..."/>
    </fieldset>
    <fieldset>
        <label for="email">Email</label>
        <input type="text" id="email" name="email" placeholder="Insert your mail..."/>
    </fieldset>
    <fieldset>
        <label for="password">Password</label>
        <input type="password" id="password" name="password" placeholder=""/>
    </fieldset>
    <fieldset>
        <label for="password">Conferma password</label>
        <input type="password" name="password-conf" placeholder=""/>
    </fieldset>

    <fieldset>
        <input type="submit" class="submit" value="Registrati"/>
    </fieldset>
</form>
<script src="../lib/vendor/jquery/jquery_1_11_2.js"></script>
<script type="text/javascript">

    /**
     * Created by filippoboiani on 29/01/16.
     */
    $(document).ready(function(){
        // Set te object to send
        //controller.php?controller=user&action=registration

        $('input.submit').on( 'click', function(e){
            console.log("click");
            e.preventDefault();
            // Get all data and crete the object
            var form = $("#registration");
            console.log(form);

            /*
             Crete object from a form: it simply maps name and values.
             */
            var data = {};
            form.serializeArray().map(function(x){data[x.name] = x.value;});
            console.log(data);
            //var serial = form.serialize();
            /*formData.action = "insert";
             formData.controller = "user";
             formData.name = $("#name").val();
             formData.lastname = $("#lastname").val();*/
            //console.log(serial);

            return sendData( data );

        });

        function sendData(dataObject) {

            console.log(dataObject);

            /*
             param()
             * Create a serialized representation of an array, a plain object, or a jQuery object suitable
             * for use in a URL query string or Ajax request. In case a jQuery object is passed, it should
             * contain input elements with name/value properties.
             * */
            data = $.param( {action: "insertUser", controller: "user", param:  dataObject} );

            console.log("data");
            console.log(data);
            $.ajax({
                type: "POST",
                dataType: "json",
                url: "../handler.php", //you can pass the type of action also response.php?action=typeofaction
                data: data,
                success: function(data) {
                    console.log("Success");
                    console.log(data);
                },
                error: function(data){
                    console.log(data);
                }
            });
            return false;
        }
    });
</script>
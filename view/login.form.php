<?php
session_start();
if($_SESSION['userId']!= null){
    echo $_SESSION['userId'];
} else {
    echo "no logged";
}
?>
<form id="login" method="post" action="">
    <fieldset>
        <label for="mail">Email</label>
        <input type="mail" id="mail" name="mail" placeholder="Insert your mail..."/>
    </fieldset>
    <fieldset>
        <label for="password">Password</label>
        <input type="password" id="password" name="password" placeholder=""/>
    </fieldset>
    <fieldset>
        <input type="submit" class="login" value="Login"/>
    </fieldset>
</form>
<script src="../lib/vendor/jquery/jquery_1_11_2.js"></script>
<script type="text/javascript">

    $(document).ready(function(){

        //set the object to send
        $('input.login').on('click', function(e){
            console.log("ready ti read login credential");

            e.preventDefault();

            //get all data from object
            var form = $("#login");

            //Create objet from a form mapping names and values
            var data = {};
            form.serializeArray().map(function(x){data[x.name] = x.value;});
            console.log(data);

            return sendLoginData(data);
        });

        function  sendLoginData(data){
            console.log("Data in sendLoginData()");

            data = {action: "loginUser", controller: "user", param: data};

            console.log("AJAX REQUEST");
            console.log(data);
            $.ajax({
                type: "POST",
                datatype: "json",
                url: "../handler.php",
                data: data,
                success: function(data){
                    console.log("SUCCESS");
                    console.log(data);
                },
                error: function(data){
                    console.log("ERROR");
                    console.log(data);
                }
            });
            return false;
        }
    });
</script>
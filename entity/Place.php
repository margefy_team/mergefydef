<?php

/**
 * Class Place
 *
 * User: Filippo Boiani
 * Date: 28/01/16
 * Time: 10:18
 */
class Place
{
    private $name;
    private $address;
    private $cap;
    private $city;
    private $nation;
    private $position;

    /**
     * Place constructor.
     * @param $name
     * @param $address
     * @param $cap
     * @param $city
     * @param $nation
     * @param Position $position
     */

    public function __construct($name, $address, $cap, $city, $nation, Position $position)
    {
        $this->name = $name;
        $this->address = $address;
        $this->cap = $cap;
        $this->city = $city;
        $this->nation = $nation;
        $this->position = $position;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Place
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     * @return Place
     */
    public function setAddress( $address )
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCap()
    {
        return $this->cap;
    }

    /**
     * @param mixed $cap
     * @return Place
     */
    public function setCap($cap)
    {
        $this->cap = $cap;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     * @return Place
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNation()
    {
        return $this->nation;
    }

    /**
     * @param mixed $nation
     * @return Place
     */
    public function setNation($nation)
    {
        $this->nation = $nation;
        return $this;
    }

    /**
     * @return Position
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param Position
     * @return Place
     */
    public function setPosition( Position $pos )
    {

        $this->position = $pos;

        return $this;
    }


}
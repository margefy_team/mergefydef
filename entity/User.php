<?php

/**
 * Class User
 *
 * Contains information about users.
 *
 * User: filippoboiani
 * Date: 24/01/16
 * Time: 17:40
 */

class User2
{
    private $id;
    private $name, $lastname;
    private $born;
    private $subscriptiondate;
    private $type;
    private $profile_pic;
    private $position;
    private $password;
    private $mail;

    /**
     * User constructor.
     * @param $id
     * @param $name
     * @param $lastname
     * @param $born
     * @param $subscriptiondate
     * @param $type
     * @param $profile_pic
     * @param $position
     * @param $password
     * @param $mail
     */
    public function __construct($id, $name, $lastname, $born, $subscriptiondate, $type, $profile_pic, $position, $password, $mail)
    {
        $this->id = $id;
        $this->name = $name;
        $this->lastname = $lastname;
        $this->born = $born;
        $this->subscriptiondate = $subscriptiondate;
        $this->type = $type;
        $this->profile_pic = $profile_pic;
        $this->position = $position;
        $this->password = $password;
        $this->mail = $mail;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @return mixed
     */
    public function getBorn()
    {
        return $this->born;
    }

    /**
     * @return mixed
     */
    public function getSubscriptiondate()
    {
        return $this->subscriptiondate;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getProfilePic()
    {
        return $this->profile_pic;
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return mixed
     */
    public function getMail()
    {
        return $this->mail;
    }




}

?>
